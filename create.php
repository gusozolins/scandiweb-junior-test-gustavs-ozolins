<?php include("inc/header.php") ?>
<?php
$connect = mysqli_connect("localhost", "root", "", "testing");
$type = '';
$attribute = '';
$query = "SELECT type FROM products GROUP BY type ORDER BY type ASC";
$result = mysqli_query($connect, $query);
while($row = mysqli_fetch_array($result))
{
 $type .= '<option value="'.$row["type"].'">'.$row["type"].'</option>';
}

$query = "SELECT attribute FROM products GROUP BY attribute";
$result = mysqli_query($connect, $query);
while($row = mysqli_fetch_array($result)){
 $attribute .= '<option value="'.$row["attribute"].'">'.$row["attribute"].'</option>';
}
?>

    <div class="container">
        <a href="index.php" class="btn btn-light mb-3"><< Go Back</a>
        <?php if (isset($_GET['status']) && $_GET['status'] == "created") : ?>
        <div class="alert alert-success" role="alert">
            <strong>Created</strong>
        </div>
        <?php endif ?>
        <?php if (isset($_GET['status']) && $_GET['status'] == "fail_create") : ?>
        <div class="alert alert-danger" role="alert">
            <strong>Fail Create</strong>
        </div>
        <?php endif ?>
        <!-- Create Form -->
        <div class="card border-danger">
            <div class="card-header bg-danger text-white">
                <strong><i class="fa fa-plus"></i> Add New Product</strong>
            </div>
            <div class="card-body">
                <form action="add.php" method="post">
                    <input type="hidden" name="id" value="<?= $products['id'] ?>">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="sku" class="col-form-label">SKU</label>
                            <input type="text" class="form-control" id="sku" name="sku" placeholder="SKU" required value="<?= $products['sku'] ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name" class="col-form-label">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name" required value="<?= $products['name'] ?>">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="price" class="col-form-label">Price</label>
                            <input type="number" class="form-control" id="price" name="price" placeholder="Price" required value="<?= $products['price'] ?>" >
                        </div>
                        <div class="form-group col-md-4">
                            <div class="container" style="width:200px;">
								<select name="type" id="type" class="form-control action">	
									<option value="">Select Category</option>
									<?php echo $type; ?>
								   </select>
								  
								   <select name="attribute" id="attribute" class="form-control action">
									<option value="">Select Attribute</option>
									<?php echo $attribute; ?>
								   </select>
								   
                        </div>
                    </div>	
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-circle"></i> Save</button>
                </form>
            </div>
        </div>
        
        <br>
    </div>
<?php include("inc/footer.php") ?>
<script> <!-- fetch the data from attribute -->
$(document).ready(function(){
 $('.action').change(function(){
  if($(this).val() != '')
  {
   var action = $(this).attr("id");
   var query = $(this).val();
   var result = '';
   if(action == "type")
   {
    result = 'attribute';
   }
   $.ajax({
    url:"fetch.php",
    method:"POST",
    data:{action:action, query:query},
    success:function(data){
     $('#'+result).html(data);
    }
   })
  }
 });
});
</script>

