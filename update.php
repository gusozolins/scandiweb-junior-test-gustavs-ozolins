<?php
require("inc/database.php");

if ($_POST) {
  $id = (int)$_POST['id'];
  $sku = trim($_POST['sku']);
  $name = trim($_POST['name']);
  $price = (float)$_POST['price'];
  $type = trim($_POST['type']);
  $attribute = trim($_POST['attribute']);

try {
  $sql = 'UPDATE products SET sku = :sku, name = :name, price = :price, type = :type, attribute = :attribute,
          WHERE id = :id';

    $stmt = $conn->prepare($sql);
    $stmt->bindParam(":sku", $sku);
    $stmt->bindParam(":name", $name);
    $stmt->bindParam(":price", $price);
    $stmt->bindParam(":type", $type);
	$stmt->bindParam(":attribute", $attribute);
    $stmt->bindParam(":id", $id);
    $stmt->execute();

if ($stmt->rowCount()) {
            header("Location: edit.php?id=".$id."&status=updated");
            exit();
        }
        header("Location: edit.php?id=".$id."&status=fail_update");
        exit();
    } catch (Exception $e) {
        echo "Error " . $e->getMessage();
        exit();
    }
} else {
    header("Location: edit.php?id=".$id."&status=fail_update");
    exit();
}
?>