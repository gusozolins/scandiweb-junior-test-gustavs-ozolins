<?php
require("inc/database.php");

if ($_POST) {

  $sku = trim($_POST['sku']);
  $name = trim($_POST['name']);
  $price = (float)$_POST['price'];
  $type = trim($_POST['type']);
$attribute = trim($_POST['attribute']);
try {
  $sql = 'INSERT INTO products(sku, name, price, type, attribute) VALUES(
    :sku, :name, :price, :type, :attribute)';

    $stmt = $conn->prepare($sql);
    $stmt->bindParam(":sku", $sku);
    $stmt->bindParam(":name", $name);
    $stmt->bindParam(":price", $price);
    $stmt->bindParam(":type", $type);
	$stmt->bindParam(":attribute", $attribute);
    $stmt->execute();

if ($stmt->rowCount()){
    header("Location: create.php?status=created");
    exit();

}
header("Location: create.php?status=fail_create");
exit();
} catch (Exception $e){
  echo "Error " . $e->getMessage();
}

} else{
  header("Location: create.php?status=fail_create");
  exit();
}

 ?>
